package com.jazed.fastgobot.handlers;

import POGOProtos.Data.PokemonDataOuterClass;
import POGOProtos.Inventory.Item.ItemIdOuterClass;
import POGOProtos.Map.Pokemon.WildPokemonOuterClass;
import com.jazed.fastgobot.account.AccountManager;
import com.jazed.fastgobot.Bot;
import com.jazed.fastgobot.util.Location;
import com.jazed.fastgobot.util.Logger;
import com.jazed.fastgobot.util.PokemonInfo;
import com.pokegoapi.api.inventory.ItemBag;
import com.pokegoapi.api.inventory.Pokeball;
import com.pokegoapi.api.map.pokemon.CatchResult;
import com.pokegoapi.api.map.pokemon.CatchablePokemon;
import com.pokegoapi.api.map.pokemon.EvolutionResult;
import com.pokegoapi.api.map.pokemon.encounter.EncounterResult;
import com.pokegoapi.api.pokemon.Pokemon;
import com.pokegoapi.api.settings.CatchOptions;
import com.pokegoapi.exceptions.LoginFailedException;
import com.pokegoapi.exceptions.NoSuchItemException;
import com.pokegoapi.exceptions.RemoteServerException;
import java.util.*;

public class PokemonHandler
{
    public static void checkForPokemon() throws LoginFailedException, RemoteServerException, NoSuchItemException
    {
        try
        {
            List<CatchablePokemon> catchable = Bot.go.getMap().getCatchablePokemon();

            if (catchable.size() > 0)
            {
                Logger.log("Found " + catchable.size() + " catchable pokemon.", Logger.Type.POKEMON);

                for (CatchablePokemon pokemon : catchable)
                {
                    if (AccountManager.getInstance().ignorePokemon(pokemon.getPokemonId().name()))
                    {
                        Logger.log("Found a " + Logger.formatName(pokemon.getPokemonId().name()) + ", skipping as it's on the ignore list.");
                        continue;
                    }

                    if (!pokemon.isEncountered())
                    {
                        EncounterResult encounter = pokemon.encounterPokemon();

                        if (encounter.wasSuccessful())
                            catchPokemon(encounter, pokemon);
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void catchPokemon(EncounterResult encounter, CatchablePokemon pokemon)
    {
        PokemonDataOuterClass.PokemonData data = encounter.getPokemonData();

        boolean isHighCp = data.getCp() > 400;
        boolean isHighPerfection = PokemonInfo.calculatePokemonPerfection(data) > AccountManager.getInstance().getKeepMinIV();

        int distance = (int)Location.calculateDistanceInMeters(Bot.go.getLatitude(), Bot.go.getLongitude(), pokemon.getLatitude(), pokemon.getLongitude());

        try
        {
            CatchOptions options = new CatchOptions(Bot.go);

            /*System.out.println("Spin modifier: " + options.getSpinModifier());
            System.out.println("Normalized hit position: " + options.getNormalizedHitPosition());
            System.out.println("Normalized reticle size: " + options.getNormalizedReticleSize());

            options.setNormalizedHitPosition(0.9D);
            options.setSpinModifier(0.3D);
            */

            if (isHighCp || isHighPerfection)
                options.useRazzberry(true);

            options.useSmartSelect(true);

            CatchResult result = pokemon.catchPokemon(options);

            int razzBerryAmount = Bot.go.getInventories().getItemBag().getItem(ItemIdOuterClass.ItemId.ITEM_RAZZ_BERRY).getCount();

            if ((isHighCp || isHighPerfection) && razzBerryAmount > 0)
                Logger.log("Using a razz berry to catch this high potential pokemon. " + razzBerryAmount + " left.", Logger.Type.INFO);

            if (!result.isFailed())
            {
                int xpGained = 0;

                for (Integer xp : result.getXpList())
                    xpGained += xp;

                AccountManager.getInstance().addXP(xpGained);
                Logger.logCatchResult(result, encounter, pokemon, distance, xpGained);
                return;
            }

            Logger.logCatchResult(result, encounter, pokemon, distance, 0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void checkForEvolves()
    {
        try
        {
            List<Pokemon> pokeBank = Bot.go.getInventories().getPokebank().getPokemons();
            HashMap<Pokemon, Integer> pokemons = new HashMap<Pokemon, Integer>();
            List<String> evolvePokemons = AccountManager.getInstance().getEvolvePokemons();

            if (pokeBank.size() <= 0)
                return;

            for (Pokemon pokemon : pokeBank)
                pokemons.put(pokemon, pokemon.getCp());

            for (Map.Entry<Pokemon, Integer> entry : sortEntriesByValues(pokemons))
            {
                Pokemon pokemon = entry.getKey();

                if (pokemon.getCandy() > pokemon.getCandiesToEvolve() && evolvePokemons.contains(pokemon.getPokemonId().name()))
                    evolvePokemon(pokemon);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void evolvePokemon(Pokemon pokemon)
    {
        try
        {
            EvolutionResult result = pokemon.evolve();

            if (result.isSuccessful())
            {
                Logger.logEvolveResult(result);
                AccountManager.getInstance().addXP(result.getExpAwarded());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void checkTransferable()
    {
        try
        {
            List<Pokemon> pokeBank = Bot.go.getInventories().getPokebank().getPokemons();
            HashMap<Pokemon, Integer> pokemons = new HashMap<Pokemon, Integer>();
            HashMap<String, Pokemon> keepPokemons = new HashMap<String, Pokemon>();

            if (pokeBank.size() <= 0)
                return;

            for (Pokemon pokemon : pokeBank)
                pokemons.put(pokemon, pokemon.getCp());

            for (Map.Entry<Pokemon, Integer> entry : sortEntriesByValues(pokemons))
            {
                Pokemon pokemon = entry.getKey();

                if (!keepPokemons.containsKey(pokemon.getPokemonId().name()))
                    keepPokemons.put(pokemon.getPokemonId().name(), pokemon);
            }

            List<Long> keepIds = new ArrayList<Long>();

            for (Map.Entry<String, Pokemon> entry : keepPokemons.entrySet())
                keepIds.add(entry.getValue().getId());

            for (Map.Entry<Pokemon, Integer> entry : pokemons.entrySet())
            {
                Pokemon pokemon = entry.getKey();

                if (!keepIds.contains(pokemon.getId()) && pokemon.getCp() < AccountManager.getInstance().getKeepMinCP() &&
                        keepPokemons.containsKey(pokemon.getPokemonId().name()))
                    transferPokemon(pokemon, keepPokemons.get(pokemon.getPokemonId().name()));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void transferPokemon(Pokemon pokemon, Pokemon bestPokemon)
    {
        try
        {
            pokemon.transferPokemon();
            Logger.logTransferResult(pokemon, bestPokemon);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static Pokemon getPokemon(long pokemodId) throws LoginFailedException, RemoteServerException
    {
        return Bot.go.getInventories().getPokebank().getPokemonById(pokemodId);
    }

    public static Pokeball calculateBestBall(double probability, WildPokemonOuterClass.WildPokemon pokemon) throws LoginFailedException, RemoteServerException
    {
        AccountManager manager = AccountManager.getInstance();
        PokemonDataOuterClass.PokemonData data = pokemon.getPokemonData();
        ItemBag itemBag = Bot.go.getInventories().getItemBag();

        int masterBallCount = itemBag.getItem(ItemIdOuterClass.ItemId.ITEM_MASTER_BALL).getCount();
        int ultraBallCount = itemBag.getItem(ItemIdOuterClass.ItemId.ITEM_ULTRA_BALL).getCount();
        int greatBallCount = itemBag.getItem(ItemIdOuterClass.ItemId.ITEM_GREAT_BALL).getCount();
        int pokeBallCount = itemBag.getItem(ItemIdOuterClass.ItemId.ITEM_POKE_BALL).getCount();

        int iv = (int)Math.round(PokemonInfo.calculatePokemonPerfection(data));

        if (data.getCp() >= 1200 && masterBallCount > 0)
            return Pokeball.MASTERBALL;

        if (data.getCp() >= 1000 && ultraBallCount > 0)
            return Pokeball.ULTRABALL;

        if (data.getCp() >= 750 && greatBallCount > 0)
            return Pokeball.GREATBALL;

        if (ultraBallCount > 0 && iv >= manager.getKeepMinIV() && probability < 0.40)
            return Pokeball.ULTRABALL;

        if (greatBallCount > 0 && iv >= manager.getKeepMinIV() && probability < 0.50)
            return Pokeball.GREATBALL;

        if (greatBallCount > 0 && data.getCp() >= 300)
            return Pokeball.GREATBALL;

        if (pokeBallCount > 0)
            return Pokeball.POKEBALL;

        if (greatBallCount > 0)
            return Pokeball.GREATBALL;

        if (ultraBallCount > 0)
            return Pokeball.ULTRABALL;

        if (masterBallCount > 0)
            return Pokeball.MASTERBALL;

        return Pokeball.POKEBALL;
    }

    private static Pokeball getNextPokeball(Pokeball pokeball)
    {
        switch (pokeball)
        {
            case POKEBALL:
                return Pokeball.GREATBALL;
            case GREATBALL:
                return Pokeball.ULTRABALL;
            case ULTRABALL:
                return Pokeball.MASTERBALL;
        }

        return Pokeball.ULTRABALL;
    }

    static <K, V extends Comparable<? super V>> List<Map.Entry<K, V>> sortEntriesByValues(Map<K, V> map)
    {
        List<Map.Entry<K, V>> sortedEntries = new ArrayList<Map.Entry<K, V>>(map.entrySet());

        Collections.sort(sortedEntries, new Comparator<Map.Entry<K, V>>()
        {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2)
            {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        return sortedEntries;
    }
}
