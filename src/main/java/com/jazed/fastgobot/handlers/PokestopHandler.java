package com.jazed.fastgobot.handlers;

import com.jazed.fastgobot.account.AccountManager;
import com.jazed.fastgobot.Bot;
import com.jazed.fastgobot.util.Location;
import com.jazed.fastgobot.util.Logger;
import com.pokegoapi.api.inventory.Item;
import com.pokegoapi.api.inventory.ItemBag;
import com.pokegoapi.api.map.MapObjects;
import com.pokegoapi.api.map.fort.Pokestop;
import com.pokegoapi.api.map.fort.PokestopLootResult;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class PokestopHandler
{
    public static void checkForPokeStops()
    {
        try
        {
            MapObjects objects = Bot.go.getMap().getMapObjects();

            Collection<Pokestop> col = objects.getPokestops();
            Iterator it = col.iterator();

            Logger.log("Found " + col.size() + " pokestops.", Logger.Type.INFO);

            int apiCalls = 0;

            while (it.hasNext())
            {
                Pokestop pokestop = (Pokestop) it.next();

                if (Location.calculateDistanceInMeters(Bot.go.getLatitude(), Bot.go.getLongitude(), pokestop.getLatitude(), pokestop.getLongitude()) <= 2000)
                    Bot.go.setLocation(pokestop.getLatitude(), pokestop.getLongitude(), 10);

                if(pokestop.canLoot() && pokestop.inRange())
                    lootPokeStop(pokestop);

                PokemonHandler.checkForPokemon();

                if (apiCalls >= 15)
                {
                    PokemonHandler.checkForEvolves();
                    PokemonHandler.checkTransferable();

                    apiCalls = 0;
                }

                apiCalls++;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void lootPokeStop(Pokestop pokestop)
    {
        int zeroXPAwarded = 0;

        try
        {
            PokestopLootResult result = pokestop.loot();

            while (zeroXPAwarded <= 50)
            {
                if (result.getExperience() == 0)
                {
                    result = pokestop.loot();
                    zeroXPAwarded++;
                    Thread.sleep(300);
                    continue;
                }

                break;
            }

            Logger.logPokestopLootResult(result, pokestop.getDetails().getName());
            recycleItems();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void recycleItems()
    {
        try
        {
            HashMap<String, Integer> itemAmounts = AccountManager.getInstance().getItemAmounts();

            ItemBag itemBag = Bot.go.getInventories().getItemBag();

            if (itemBag.getItemsCount() <= 345)
                return;

            for (Item item : itemBag.getItems())
            {
                if (itemAmounts.containsKey(item.getItemId().name()))
                {
                    if (item.getCount() > itemAmounts.get(item.getItemId().name()))
                    {
                        Logger.log("(Recycling) " + (item.getCount() - itemAmounts.get(item.getItemId().name())) + "x " + Logger.formatName(item.getItemId().name()), Logger.Type.TRANSFER);
                        itemBag.removeItem(item.getItemId(), item.getCount() - itemAmounts.get(item.getItemId().name()));
                    }
                }
                else if (item.getCount() > 0)
                {
                    Logger.log("(Recycling) " + item.getCount() + "x " + Logger.formatName(item.getItemId().name()), Logger.Type.TRANSFER);
                    itemBag.removeItem(item.getItemId(), item.getCount());
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
