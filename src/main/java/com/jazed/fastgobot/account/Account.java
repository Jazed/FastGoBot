package com.jazed.fastgobot.account;

import org.json.JSONObject;

public class Account
{
    private String username, password;
    private JSONObject settings;
    private int targetLevel;

    public Account(String username, String password, JSONObject settings, int targetLevel)
    {
        this.username = username;
        this.password = password;
        this.settings = settings;
        this.targetLevel = targetLevel;
    }

    public String getUsername()
    {
        return this.username;
    }

    public String getPassword()
    {
        return this.password;
    }

    public JSONObject getSettings()
    {
        return this.settings;
    }

    public int getTargetLevel()
    {
        return this.targetLevel;
    }
}
