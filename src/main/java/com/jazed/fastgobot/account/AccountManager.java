package com.jazed.fastgobot.account;

import com.jazed.fastgobot.Bot;
import com.jazed.fastgobot.util.Config;
import com.jazed.fastgobot.tasks.TaskHandler;
import com.jazed.fastgobot.util.Logger;
import com.pokegoapi.api.PokemonGo;
import com.pokegoapi.api.device.DeviceInfo;
import com.pokegoapi.auth.PtcCredentialProvider;
import okhttp3.OkHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class AccountManager
{
    private static AccountManager instance = new AccountManager();

    private OkHttpClient httpClient = new OkHttpClient();

    private LoginType loginType = null;
    private String refreshToken = null;

    private double startLat, startLng, startAlt = 10;
    private int keepMinCP, keepMinIV;
    private int totalXP = 0;
    private long startTime;

    private HashMap<String, Integer> itemAmounts = new HashMap<>();
    private List<String> evolvePokemons = new ArrayList<>();
    private List<String> ignorePokemons = new ArrayList<>();

    private List<Account> accounts = new ArrayList<>();
    private Account currentAccount = null;

    public enum LoginType
    {
        PTC, GOOGLE
    }

    public static AccountManager getInstance()
    {
        return instance;
    }

    public boolean startAccount()
    {
        this.currentAccount = getNextAccount();

        if (this.currentAccount == null)
        {
            Logger.log("Couldn't find any accounts to level, make sure you have some added in the panel.", Logger.Type.SEVERE);
            Config.sleep();
        }

        Logger.log("Grabbed account: " + this.currentAccount.getUsername() + " training to level: " + this.currentAccount.getTargetLevel(), Logger.Type.INFO);

        try
        {
            JSONObject settings = this.currentAccount.getSettings();

            this.startLat = settings.getDouble("latitude");
            this.startLng = settings.getDouble("longitude");

            this.keepMinCP = 1250;
            this.keepMinIV = 90;
            this.loginType = LoginType.PTC;

            JSONArray array = settings.getJSONArray("evolvePokemons");

            for (int i = 0; i < array.length(); i++)
                this.evolvePokemons.add(array.get(i).toString().toUpperCase());

            array = settings.getJSONArray("ignorePokemons");

            for (int i = 0; i < array.length(); i++)
                this.ignorePokemons.add(array.get(i).toString().toUpperCase());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        //this.httpClient.proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1", 1337)));

        this.startTime = System.currentTimeMillis();

        this.itemAmounts.put("ITEM_POKE_BALL", 50);
        this.itemAmounts.put("ITEM_GREAT_BALL", 50);
        this.itemAmounts.put("ITEM_ULTRA_BALL", 100);
        this.itemAmounts.put("ITEM_MASTER_BALL", 100);
        this.itemAmounts.put("ITEM_POTION", 0);
        this.itemAmounts.put("ITEM_SUPER_POTION", 0);
        this.itemAmounts.put("ITEM_HYPER_POTION", 0);
        this.itemAmounts.put("ITEM_MAX_POTION", 50);
        this.itemAmounts.put("ITEM_REVIVE", 0);
        this.itemAmounts.put("ITEM_MAX_REVIVE", 50);
        this.itemAmounts.put("ITEM_LUCKY_EGG", 50);
        this.itemAmounts.put("ITEM_INCENSE_ORDINARY", 50);
        this.itemAmounts.put("ITEM_RAZZ_BERRY", 50);
        this.itemAmounts.put("ITEM_SPECIAL_CAMERA", 5);
        this.itemAmounts.put("ITEM_INCUBATOR_BASIC_UNLIMITED", 9);
        this.itemAmounts.put("ITEM_INCUBATOR_BASIC", 9);

        try
        {
            if (this.loginType.equals(LoginType.GOOGLE))
            {
                if (this.refreshToken == null)
                {
                    //GoogleUserCredentialProvider provider = new GoogleUserCredentialProvider(this.getHttpClient());
                    //Bot.go = new PokemonGo(new GoogleCredentialProvider(this.httpClient, new GoogleLoginListener()), httpClient);

                    Logger.log("Logged in with Google.", Logger.Type.INFO);
                    return true;
                }
            }
            else
            {
                String uuid = UUID.randomUUID().toString();
                Random random = new Random(uuid.hashCode());
                String[][] devices =
                        {
                                {"iPad3,1", "iPad", "J1AP"},
                                {"iPad3,2", "iPad", "J2AP"},
                                {"iPad3,3", "iPad", "J2AAP"},
                                {"iPad3,4", "iPad", "P101AP"},
                                {"iPad3,5", "iPad", "P102AP"},
                                {"iPad3,6", "iPad", "P103AP"},

                                {"iPad4,1", "iPad", "J71AP"},
                                {"iPad4,2", "iPad", "J72AP"},
                                {"iPad4,3", "iPad", "J73AP"},
                                {"iPad4,4", "iPad", "J85AP"},
                                {"iPad4,5", "iPad", "J86AP"},
                                {"iPad4,6", "iPad", "J87AP"},
                                {"iPad4,7", "iPad", "J85mAP"},
                                {"iPad4,8", "iPad", "J86mAP"},
                                {"iPad4,9", "iPad", "J87mAP"},

                                {"iPad5,1", "iPad", "J96AP"},
                                {"iPad5,2", "iPad", "J97AP"},
                                {"iPad5,3", "iPad", "J81AP"},
                                {"iPad5,4", "iPad", "J82AP"},

                                {"iPad6,7", "iPad", "J98aAP"},
                                {"iPad6,8", "iPad", "J99aAP"},

                                {"iPhone5,1", "iPhone", "N41AP"},
                                {"iPhone5,2", "iPhone", "N42AP"},
                                {"iPhone5,3", "iPhone", "N48AP"},
                                {"iPhone5,4", "iPhone", "N49AP"},

                                {"iPhone6,1", "iPhone", "N51AP"},
                                {"iPhone6,2", "iPhone", "N53AP"},

                                {"iPhone7,1", "iPhone", "N56AP"},
                                {"iPhone7,2", "iPhone", "N61AP"},

                                {"iPhone8,1", "iPhone", "N71AP"}

                        };
                String[] osVersions = {"8.1.1", "8.1.2", "8.1.3", "8.2", "8.3", "8.4", "8.4.1",
                        "9.0", "9.0.1", "9.0.2", "9.1", "9.2", "9.2.1", "9.3", "9.3.1", "9.3.2", "9.3.3", "9.3.4"};

                DeviceInfo info = new DeviceInfo();

                String[] device = devices[random.nextInt(devices.length)];
                info.setDeviceId(uuid);//"04cfc666-a27d-494f-8735-f6b5a6190d24"
                info.setDeviceBrand("Apple");
                info.setDeviceModel(device[1]);//"iPhone"
                info.setDeviceModelBoot(device[0]);//"iPhone8,1"
                info.setHardwareManufacturer("Apple");
                info.setHardwareModel(device[2]);//"N71AP"
                info.setFirmwareBrand("iPhone OS");
                info.setFirmwareType(osVersions[random.nextInt(osVersions.length)]);//"9.3.4"

                System.out.println(uuid + " | " + device[1] + " | " + device[0] + " | " + device[2]);

                Bot.go = new PokemonGo(httpClient);
                Bot.go.login(new PtcCredentialProvider(httpClient, this.currentAccount.getUsername(), this.currentAccount.getPassword()));
                Bot.go.setDeviceInfo(info);
                Bot.go.setLocation(this.startLat, this.startLng, this.startAlt);

                Logger.log("Logged in with PTC.", Logger.Type.INFO);
                Logger.log("Starting at location: " + this.startLat + ", " + this.startLng + ".", Logger.Type.INFO);

                Thread.sleep(2000);

                Logger.log("Current level: " + Bot.go.getPlayerProfile().getStats().getLevel() + "/" + this.currentAccount.getTargetLevel() + " | Pokebank: " +
                        Bot.go.getInventories().getPokebank().getPokemons().size() +
                        " | Item bag: " + Bot.go.getInventories().getItemBag().getItemsCount(), Logger.Type.INFO);

                Bot.gui.setUsername(this.currentAccount.getUsername());

                new Thread(TaskHandler.getInstance().getUpdateTask()).start();
                TaskHandler.getInstance().setup();
                return true;
            }
        }
        catch (Exception e)
        {
            Logger.log("An error occurred while trying to login through PTC... servers down?", Logger.Type.SEVERE);
            e.printStackTrace();
        }

        return false;
    }

    public double getStartLat()
    {
        return startLat;
    }

    public double getStartLng()
    {
        return startLng;
    }

    public int getKeepMinCP()
    {
        return this.keepMinCP;
    }

    public int getKeepMinIV()
    {
        return this.keepMinIV;
    }

    public void addXP(int amount)
    {
        this.totalXP += amount;
    }

    public int getTotalXP()
    {
        return this.totalXP;
    }

    public long getStartTime()
    {
        return this.startTime;
    }

    public HashMap<String, Integer> getItemAmounts()
    {
        return this.itemAmounts;
    }

    public List<String> getEvolvePokemons()
    {
        return this.evolvePokemons;
    }

    public boolean ignorePokemon(String pokemon)
    {
        return this.ignorePokemons.contains(pokemon);
    }

    public void addAccount(String username, String password, JSONObject settings, int targetLevel)
    {
        this.accounts.add(new Account(username, password, settings, targetLevel));
    }

    public Account getNextAccount()
    {
        Account account = null;

        if (this.accounts.size() > 0)
        {
            account = this.accounts.get(0);
            this.accounts.remove(0);
        }

        return account;
    }

    public Account getCurrentAccount()
    {
        return this.currentAccount;
    }
}