package com.jazed.fastgobot.api;

import com.jazed.fastgobot.util.Config;
import com.jazed.fastgobot.account.AccountManager;
import com.jazed.fastgobot.util.Logger;
import com.jazed.fastgobot.util.SystemUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.*;

public class Api
{
    private static Api instance = new Api();

    private Status currentStatus = Status.IDLE;

    private String UID = null, username, password;

    private String charset = "UTF-8";
    private String apiURL = "https://auth.fastgobot.com/api";

    public enum Status
    {
        EVOLVING, TELEPORTING, CATCHING, TRANSFERING, IDLE
    }

    public static Api getInstance()
    {
        return instance;
    }

    public boolean login(String username, String password)
    {
        final JSONObject ob = new JSONObject();

        try
        {
            ob.put("u", username);
            ob.put("p", password);
            ob.put("v", Config.version);

            String screenshot = SystemUtils.getConsoleImage();

            if (screenshot != null)
                ob.put("i", encodeURLString(screenshot));

            String response = httpPOSTRequest(generateQueryString(new HashMap<String, String>()
            {{
                put("a", "login");
                put("j", ob.toString());
            }}));

            if (response != null)
            {
                JSONObject json = new JSONObject(response);

                if (!json.isNull("error"))
                {
                    Logger.log(json.getString("error"), Logger.Type.SEVERE);
                    return false;
                }

                if (!json.getBoolean("login"))
                    return false;

                this.UID = json.getString("uid");
                this.username = username;
                this.password = password;

                //TODO: Encrypt data between server and client.

                if (!json.isNull("accounts"))
                {
                    JSONArray arr = json.getJSONArray("accounts");

                    for (int i = 0; i < arr.length(); i++)
                    {
                        JSONObject account = arr.getJSONObject(i);

                        AccountManager.getInstance().addAccount("sdouglas906", "sdouglas506",
                                account.getJSONObject("settings"), account.getInt("targetLevel"));//account.getString("username"), account.getString("password")
                    }
                }

                Logger.log("Successful login, UID: " + this.UID, Logger.Type.INFO);
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public String httpGETRequest()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            String inputLine;

            URL conn = new URL(this.apiURL);
            URLConnection yc = conn.openConnection();

            yc.setConnectTimeout(10000);
            yc.setReadTimeout(10000);
            yc.setRequestProperty("User-Agent", "PoGoBoat");

            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));

            while ((inputLine = in.readLine()) != null)
                sb.append(inputLine);

            in.close();

            return sb.toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public String httpPOSTRequest(String query)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            String inputLine;

            URL conn = new URL(this.apiURL);
            URLConnection yc = conn.openConnection();

            yc.setDoOutput(true);
            yc.setConnectTimeout(10000);
            yc.setReadTimeout(10000);
            yc.setRequestProperty("User-Agent", "PoGoBot");
            yc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            yc.setRequestProperty("charset", this.charset);
            yc.setRequestProperty("Content-Length", String.valueOf(query.getBytes(this.charset).length));

            OutputStream os = yc.getOutputStream();

            os.write(query.getBytes(this.charset));
            os.flush();
            os.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));

            while ((inputLine = in.readLine()) != null)
                sb.append(inputLine);

            in.close();

            System.out.println("Debug: " + sb.toString().replaceAll("<br />", "\n"));

            return sb.toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public String generateQueryString(HashMap<String, String> map)
    {
        StringBuilder sb = new StringBuilder();

        for (Map.Entry<String, String> entry : map.entrySet())
        {
            if (sb.length() > 0)
                sb.append("&");

            try
            {
                sb.append(String.format("%s=%s", encodeURLString(entry.getKey()), encodeURLString(entry.getValue())));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    public String encodeURLString(String encode)
    {
        try
        {
            return URLEncoder.encode(encode, this.charset);
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public void setStatus(Status status)
    {
        this.currentStatus = status;
    }

    public String getUID()
    {
        return this.UID;
    }

    public String getUsername()
    {
        return this.username;
    }

    public String getPassword()
    {
        return this.password;
    }
}
