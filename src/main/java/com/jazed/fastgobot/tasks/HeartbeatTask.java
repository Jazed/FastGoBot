package com.jazed.fastgobot.tasks;

import com.jazed.fastgobot.Bot;
import com.jazed.fastgobot.account.Account;
import com.jazed.fastgobot.account.AccountManager;
import com.jazed.fastgobot.api.Api;
import com.jazed.fastgobot.util.Logger;
import com.jazed.fastgobot.util.SystemUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.TimerTask;

public class HeartbeatTask extends TimerTask
{
    private Api api = Api.getInstance();

    public void run()
    {
        final String UID = api.getUID();

        if (UID != null)
        {
            Account account = AccountManager.getInstance().getCurrentAccount();
            final JSONObject ob = new JSONObject();

            try
            {
                ob.put("u", UID);
                ob.put("c", String.valueOf(SystemUtils.getCPUUsage()));
                ob.put("m", String.valueOf(SystemUtils.getMemUsage()));

                if (account != null && Bot.go != null)
                {
                    ob.put("a", account.getUsername());
                    ob.put("l", Bot.go.getPlayerProfile().getStats().getLevel());
                }

                String screenshot = SystemUtils.getConsoleImage();

                if (screenshot != null)
                    ob.put("i", Api.getInstance().encodeURLString(screenshot));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            String response = api.httpPOSTRequest(api.generateQueryString(new HashMap<String, String>()
            {{
                put("a", "heartbeat");
                put("j", ob.toString());
            }}));

            if (response != null)
            {
                try
                {
                    JSONObject json = new JSONObject(response);

                    if (!json.isNull("e"))
                    {
                        Logger.log("Heartbeat code received: " + json.getInt("e"), Logger.Type.SEVERE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
