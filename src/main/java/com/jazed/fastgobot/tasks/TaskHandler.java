package com.jazed.fastgobot.tasks;

import java.util.Timer;

public class TaskHandler
{
    private static TaskHandler instance = new TaskHandler();

    private Timer timer = new Timer();
    private UpdateTask updateTask = new UpdateTask();

    public static TaskHandler getInstance()
    {
        return instance;
    }

    public void setup()
    {
        this.timer.scheduleAtFixedRate(new HeartbeatTask(), 0, 5 * (60 * 1000));
        this.timer.scheduleAtFixedRate(new StatsTask(), 0, 10 * 1000);
    }

    public UpdateTask getUpdateTask()
    {
        return this.updateTask;
    }
}
