package com.jazed.fastgobot.tasks;

import com.jazed.fastgobot.Bot;
import com.jazed.fastgobot.handlers.PokemonHandler;
import com.jazed.fastgobot.handlers.PokestopHandler;
import com.jazed.fastgobot.util.Logger;

public class UpdateTask implements Runnable
{
    public void run()
    {
        try
        {
            Logger.log("Starting bot.");

            while (Bot.isRunning())
            {
                if (Bot.go != null)
                {
                    PokemonHandler.checkForPokemon();
                    PokestopHandler.checkForPokeStops();
                    PokemonHandler.checkForEvolves();
                    PokemonHandler.checkTransferable();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
