package com.jazed.fastgobot.tasks;

import com.jazed.fastgobot.Bot;
import com.jazed.fastgobot.account.Account;
import com.jazed.fastgobot.account.AccountManager;
import com.jazed.fastgobot.util.Logger;
import com.pokegoapi.api.inventory.Stats;

import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class StatsTask extends TimerTask
{
    public void run()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            long startTime = AccountManager.getInstance().getStartTime();

            int xpGained = AccountManager.getInstance().getTotalXP();
            int runTime = (int)((System.currentTimeMillis() - startTime) / 1000) / 60;
            int xpPerHour = (runTime > 0) ? (60 * xpGained) / runTime : 0;

            Account account = AccountManager.getInstance().getCurrentAccount();
            Stats stats = Bot.go.getPlayerProfile().getStats();

            sb.append(getRunTime(System.currentTimeMillis() - startTime));
            sb.append(" | ");
            sb.append("XP gained: " + Logger.formatNumber(xpGained));
            sb.append(" | ");
            sb.append("XP/hr: " + ((xpPerHour > 0 ) ? Logger.formatNumber(xpPerHour) : "Calculating..."));
            sb.append(" | ");
            sb.append("Current XP: " + Logger.formatNumber((int)stats.getExperience()) + "/" + Logger.formatNumber((int)stats.getNextLevelXp()));
            sb.append(" | ");
            sb.append("Level: " + stats.getLevel());

            if (account != null)
                sb.append("/" + account.getTargetLevel());

            if (Bot.gui != null)
                Bot.gui.setStats(sb.toString());

            //Logger.log(sb.toString(), Logger.Type.SEVERE);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private String getRunTime(long startTime)
    {
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(startTime),
                TimeUnit.MILLISECONDS.toMinutes(startTime) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(startTime)),
                TimeUnit.MILLISECONDS.toSeconds(startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(startTime)));
    }
}
