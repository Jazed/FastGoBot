package com.jazed.fastgobot;

import com.jazed.fastgobot.util.Config;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class GUI extends JFrame
{
    private StyledDocument doc;
    private Style style;

    private JTextPane consoleOutput;
    private JScrollPane scrollPane;
    private JLabel stats;
    private JLabel username;

    public GUI()
    {
        initComponents();

        this.getContentPane().setBackground(new Color(60, 63, 65));

        this.doc = this.consoleOutput.getStyledDocument();
        this.style = this.consoleOutput.addStyle("Style", null);

        try
        {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                setLocationRelativeTo(null);
                setVisible(true);
            }
        });
    }

    public void appendToPane(String msg, Color c)
    {
        try
        {
            StyleConstants.setForeground(this.style, c);
            doc.insertString(doc.getLength(), msg + "\n", this.style);
            this.consoleOutput.setCaretPosition(doc.getLength());
        }
        catch (BadLocationException ex)
        {
        }
    }

    public void setStats(String stats)
    {
        this.stats.setText(stats);
    }

    public void setUsername(String username)
    {
        this.username.setText(username);
    }

    @SuppressWarnings("unchecked")
    private void initComponents()
    {
        username = new JLabel();
        scrollPane = new JScrollPane();
        consoleOutput = new JTextPane();
        stats = new JLabel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("FastGoBot v" + Config.version);
        setBackground(new Color(60, 63, 65));
        setMinimumSize(new Dimension(1000, 381));

        username.setFont(new Font("Century Gothic", 0, 24));
        username.setForeground(new Color(255, 255, 255));
        username.setHorizontalAlignment(SwingConstants.CENTER);
        username.setText("Loading");

        scrollPane.setBackground(new Color(43, 43, 43));
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        consoleOutput.setEditable(false);
        consoleOutput.setBackground(new Color(43, 43, 43));
        consoleOutput.setFont(new Font("Monospaced", 0, 12));
        consoleOutput.setForeground(new Color(255, 255, 255));
        scrollPane.setViewportView(consoleOutput);

        stats.setFont(new Font("Century Gothic", 0, 14));
        stats.setForeground(new Color(255, 255, 255));
        stats.setText("Loading...");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(scrollPane)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(username)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(stats, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 267, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(username)
                                        .addComponent(stats, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
                                .addContainerGap())
        );

        pack();
    }
}
