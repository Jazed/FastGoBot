package com.jazed.fastgobot;

import com.jazed.fastgobot.account.AccountManager;
import com.jazed.fastgobot.api.Api;
import com.jazed.fastgobot.util.Config;
import com.jazed.fastgobot.util.Logger;
import com.pokegoapi.api.PokemonGo;
import com.pokegoapi.util.Log;

public class Bot
{
    private static boolean run = true;
    //TODO: Only print stackTrace is debug is enabled, add checks in exception handling.
    public static boolean debug = false;

    public static PokemonGo go = null;
    public static GUI gui = null;

    public static void main(String[] args)
    {
        boolean useGUI = true;
        String username = "";
        String password = "";

        Log.setLevel(Log.Level.NONE);

        if (args.length > 0)
        {
            for (int i = 0; i < args.length; i++)
            {
                switch (args[i])
                {
                    case "--nogui":
                        useGUI = false;
                        continue;
                    case "--debug":
                        debug = true;
                        continue;
                }

                if (args[i].contains("--user="))
                {
                    username = args[i].split("=")[1];
                    continue;
                }

                if (args[i].contains("--pass="))
                {
                    password = args[i].split("=")[1];
                    continue;
                }
            }
        }

        if (useGUI)
            gui = new GUI();

        if (username.isEmpty() || password.isEmpty())
        {
            String[] credentials = Config.readConfig();

            if (credentials == null)
            {
                Logger.log("Invalid credentials, please check username & password in the config file, they are CaSe SeNsItIvE.", Logger.Type.SEVERE);
                Config.sleep();
            }

            username = credentials[0];
            password = credentials[1];
        }

        Logger.log("Starting bot, grabbing account information.", Logger.Type.INFO);

        if (!Api.getInstance().login(username, password))
            Config.sleep();

        AccountManager.getInstance().startAccount();
    }

    public static boolean isRunning()
    {
        return run;
    }
}
