package com.jazed.fastgobot.util;

import com.grum.geocalc.DegreeCoordinate;
import com.grum.geocalc.EarthCalc;
import com.grum.geocalc.Point;

public class Location
{
    public static double calculateDistanceInMeters(double lat, double lng, double lat2, double lng2)
    {
        Point point1 = new Point(new DegreeCoordinate(lat), new DegreeCoordinate(lng));
        Point point2 = new Point(new DegreeCoordinate(lat2), new DegreeCoordinate(lng2));

        return EarthCalc.getDistance(point1, point2);
    }
}
