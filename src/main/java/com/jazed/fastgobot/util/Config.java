package com.jazed.fastgobot.util;

import org.ini4j.Ini;
import org.ini4j.Profile;

import java.io.File;
public class Config
{
    public final static String ptcUsername = "jsmith740";
    public final static String ptcPassword = "jsmith753";
	
	public final static double version = 1.0;

    public final static String configFile = System.getProperty("user.dir") + System.getProperty("file.separator") + "fastgobot_config.ini";

    public static String[] readConfig()
    {
        Ini ini = checkConfig();

        if (ini != null)
        {
            Profile.Section credentials = ini.get("credentials");

            if (credentials != null && credentials.containsKey("username") && credentials.containsKey("password"))
            {
                String[] creds = new String[2];

                creds[0] = credentials.get("username");
                creds[1] = credentials.get("password");

                Logger.log("Username: " + creds[0] + " | Password: " + creds[1], Logger.Type.INFO);

                return creds;
            }
        }

        return null;
    }

    private static Ini checkConfig()
    {
        try
        {
            File file = new File(configFile);

            if (file.exists())
                return new Ini(file);

            Ini ini = new Ini();

            ini.setComment("FastGoBot config");
            ini.putComment("credentials", "Your panel username and password, used for validation.");
            ini.put("credentials", "username", "PanelUsername");
            ini.put("credentials", "password", "PanelPassword");

            ini.store(file);

            Logger.log("It looks like you're running a clean installation, the bot has generated a 'fastgobot_config.ini' file. " +
                    "Please edit the username & password to your panel login credentials. After that save & reboot the bot. " +
                    "If you don't do anything the bot will close itself in 5 minutes.", Logger.Type.SEVERE);

            sleep();
        }
        catch (Exception e)
        {
            Logger.log("There appears to be something wrong with your config file, try deleting it and letting the bot regenerate it.", Logger.Type.SEVERE);
            e.printStackTrace();
            sleep();
        }

        return null;
    }

    public static void sleep()
    {
        try
        {
            Thread.sleep((1000 * 60) * 5);
            System.exit(0);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
