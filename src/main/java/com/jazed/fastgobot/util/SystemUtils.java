package com.jazed.fastgobot.util;

import com.jazed.fastgobot.Bot;
import com.jazed.fastgobot.GUI;
import net.iharder.Base64;
import oshi.SystemInfo;
import oshi.hardware.GlobalMemory;
import oshi.hardware.HardwareAbstractionLayer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

public class SystemUtils
{
    private static SystemInfo system = new SystemInfo();
    private static HardwareAbstractionLayer hal = system.getHardware();
    private static GlobalMemory mem = hal.getMemory();

    public static double getCPUUsage()
    {
        return hal.getProcessor().getSystemCpuLoad() * 100;
    }

    public static double getMemUsage()
    {
        return (100 - ((double)mem.getAvailable() / mem.getTotal()) * 100);
    }

    public static String getConsoleImage()
    {
        if (Bot.gui == null)
            return null;

        try
        {
            GUI gui = Bot.gui;

            BufferedImage image = new BufferedImage(
                    gui.getRootPane().getWidth(),
                    gui.getRootPane().getHeight(),
                    BufferedImage.TYPE_INT_RGB
            );

            gui.getRootPane().paint(image.getGraphics());

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            OutputStream os = new Base64.OutputStream(out);

            ImageIO.write(image, "png", os);

            return out.toString("UTF-8");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
