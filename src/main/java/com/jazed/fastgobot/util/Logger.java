package com.jazed.fastgobot.util;

import POGOProtos.Data.PokemonDataOuterClass;
import POGOProtos.Inventory.Item.ItemAwardOuterClass;
import com.jazed.fastgobot.Bot;
import com.jazed.fastgobot.account.AccountManager;
import com.pokegoapi.api.map.fort.PokestopLootResult;
import com.pokegoapi.api.map.pokemon.CatchResult;
import com.pokegoapi.api.map.pokemon.CatchablePokemon;
import com.pokegoapi.api.map.pokemon.EvolutionResult;
import com.pokegoapi.api.map.pokemon.encounter.EncounterResult;
import com.pokegoapi.api.pokemon.Pokemon;
import com.pokegoapi.exceptions.LoginFailedException;
import com.pokegoapi.exceptions.RemoteServerException;

import java.awt.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class Logger
{
    private static DateFormat df = new SimpleDateFormat("HH:mm:ss");
    private static DecimalFormat nf = new DecimalFormat("##.##");
    private static DecimalFormat bnf = new DecimalFormat("###,###,###");

    private static HashMap<Type, ColorType> colors = new HashMap<Type, ColorType>()
    {{
        put(Type.NORMAL, new ColorType(Color.WHITE, WHITE));
        put(Type.INFO, new ColorType(new Color(83, 148, 236), BLUE));
        put(Type.SEVERE, new ColorType(new Color(255, 106, 92), RED));
        put(Type.POKEMON, new ColorType(new Color(168, 192, 35), GREEN));
        put(Type.POKESTOP, new ColorType(new Color(41, 136, 152), CYAN));
        put(Type.TRANSFER, new ColorType(new Color(174, 138, 190), PURPLE));
        put(Type.EVOLVE, new ColorType(new Color(214, 191, 85), YELLOW));
    }};

    public enum Type
    {
        NORMAL, INFO, SEVERE, POKEMON, POKESTOP, TRANSFER, EVOLVE
    }

    public static void log(String string, Type type)
    {
        String currentTime = getCurrentTime();
        ColorType colorType = colors.get(type);

        if (Bot.gui != null)
            Bot.gui.appendToPane("[" + currentTime + "] " + string, colorType.getGUIcolor());

        System.out.println(colorType.getConsoleColor() + "[" + currentTime + "] " + string + RESET);
    }

    public static void log(String string)
    {
        log(string, Type.INFO);
    }

    public static void logCatchResult(CatchResult result, EncounterResult encounter, CatchablePokemon pokemon, int distance, int xpGained)
            throws LoginFailedException, RemoteServerException
    {
        StringBuilder sb = new StringBuilder();
        PokemonDataOuterClass.PokemonData data = encounter.getPokemonData();

        sb.append("(Pokemon) ");
        sb.append("(" + formatName(result.getStatus().toString()) + ") ");
        sb.append(formatName(pokemon.getPokemonId().name()) + " ");
        sb.append("CP: (" + data.getCp() + "/" + PokemonInfo.calculateMaxCp(data) + ") | ");
        sb.append("IV: " + nf.format(PokemonInfo.calculatePokemonPerfection(data)) + "% | ");
        sb.append("Distance: " + distance + "m | ");
        sb.append("Candies: " + (-1) + " | ");
        sb.append("XP: " + xpGained);

        log(sb.toString(), Type.POKEMON);
    }

    public static void logPokestopLootResult(PokestopLootResult result, String name)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("(Pokestop) ");
        sb.append("Name: " + name);
        sb.append(", XP: " + result.getExperience());
        sb.append(", Items: ");

        for (ItemAwardOuterClass.ItemAward award : result.getItemsAwarded())
        {
            sb.append(award.getItemCount());
            sb.append("x ");
            sb.append(formatName(award.getItemId().name()));
            sb.append(", ");
        }

        AccountManager.getInstance().addXP(result.getExperience());
        log(sb.toString().substring(0, sb.toString().length() - 2), Type.POKESTOP);
    }

    public static void logEvolveResult(EvolutionResult result)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("(Evolved) ");
        sb.append(formatName(result.getEvolvedPokemon().getMeta().getParentId().name()) + " ");
        sb.append("successfully for " + result.getExpAwarded() + "xp.");

        log(sb.toString(), Type.EVOLVE);
    }

    public static void logTransferResult(Pokemon pokemon, Pokemon bestPokemon) throws LoginFailedException, RemoteServerException
    {
        StringBuilder sb = new StringBuilder();

        sb.append("(Transferred) ");
        sb.append(formatName(pokemon.getPokemonId().name()) + " ");
        sb.append("CP: " + pokemon.getCp() + " | ");
        sb.append("IV: " + nf.format(PokemonInfo.calculatePokemonPerfection(pokemon.getDefaultInstanceForType())) + "% | ");
        sb.append("[Best CP: " + bestPokemon.getCp() + " IV: " + nf.format(pokemon.getIvRatio()) + "] | ");
        sb.append("Candies: " + bestPokemon.getCandy());

        Logger.log(sb.toString(), Type.TRANSFER);
    }

    private static String getCurrentTime()
    {
        return df.format(Calendar.getInstance().getTime());
    }

    public static String formatName(String itemName)
    {
        StringBuilder sb = new StringBuilder();
        String[] parts = itemName.split("_");

        if (parts.length > 0)
        {
            for (String part : parts)
            {
                if (sb.length() > 0)
                    sb.append(" ");

                sb.append(part.substring(0, 1));
                sb.append(part.substring(1, part.length()).toLowerCase());
            }

            return sb.toString();
        }

        sb.append(itemName.substring(0, 1));
        sb.append(itemName.substring(1, itemName.length()).toLowerCase());

        return sb.toString();
    }

    public static String formatNumber(int number)
    {
        return bnf.format(number);
    }

    public static DecimalFormat getDF()
    {
        return nf;
    }

    private static final String RESET = "\u001B[0m";
    private static final String RED = "\u001B[31m";
    private static final String GREEN = "\u001B[32m";
    private static final String YELLOW = "\u001B[33m";
    private static final String BLUE = "\u001B[1;34m";
    private static final String PURPLE = "\u001B[35m";
    private static final String CYAN = "\u001B[36m";
    private static final String WHITE = "\u001B[37m";

    private static class ColorType
    {
        private Color GUIcolor;
        private String consoleColor;

        public ColorType(Color GUIcolor, String consoleColor)
        {
            this.GUIcolor = GUIcolor;
            this.consoleColor = consoleColor;
        }

        public Color getGUIcolor()
        {
            return this.GUIcolor;
        }

        public String getConsoleColor()
        {
            return this.consoleColor;
        }
    }
}
